const faker = require('faker');

test('transaction creates has properties', () => {
  const transaction = faker.helpers.createTransaction();

  expect(transaction).toMatchObject({
    account: expect.any(String),
    amount: expect.any(String),
    business: expect.any(String),
    date: expect.any(Date),
    name: expect.any(String),
    type: expect.any(String),
  });
});

test('create unique instance of transaction', () => {
  const transaction1 = faker.helpers.createTransaction();
  const transaction2 = faker.helpers.createTransaction();

  expect(transaction1).not.toStrictEqual(transaction2);
});
// END
