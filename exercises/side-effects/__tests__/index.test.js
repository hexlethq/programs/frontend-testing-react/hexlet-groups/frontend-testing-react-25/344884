const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

const getFixturePath = (filename) => path.join(path.resolve(), '__fixtures__', filename);
const readFile = async (filename) => await fs.readFileSync(getFixturePath(filename), 'utf-8');

describe('upVersion test', () => {
  let before;

  beforeAll(async () => {
    before = await readFile('package.json');
  });

  afterEach(async () => {
    await fs.writeFileSync(getFixturePath('package.json'), before);
  });

  it('should change with default', async () => {
    upVersion(getFixturePath('package.json'));
    const updated = await readFile('package.json');
    const { version } = JSON.parse(updated);
    expect(version).toEqual('1.3.3');
  });

  it('should change with minor', async () => {
    upVersion(getFixturePath('package.json'), 'minor');
    const updated = await readFile('package.json');
    const { version } = JSON.parse(updated);
    expect(version).toEqual('1.4.2');
  });

  it('should change with major', async () => {
    upVersion(getFixturePath('package.json'), 'major');
    const updated = await readFile('package.json');
    const { version } = JSON.parse(updated);
    expect(version).toEqual('2.3.2');
  });
});
