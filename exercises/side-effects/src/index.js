const fs = require('fs');

const UpdateType = {
  PATCH: 'patch',
  MINOR: 'minor',
  MAJOR: 'major',
};

const upVersion = async (dir, updateType = UpdateType.PATCH) => {
  const content = fs.readFileSync(dir, 'utf-8');
  const json = JSON.parse(content);
  let [major, minor, patch] = json.version.split('.').map(Number);

  switch (updateType) {
    case UpdateType.PATCH: {
      patch++;
      break;
    }
    case UpdateType.MINOR: {
      minor++;
      break;
    }
    case UpdateType.MAJOR: {
      major++;
      break;
    }
  }
  json.version = [major, minor, patch].map(String).join('.');
  await fs.writeFileSync(dir, JSON.stringify(json, null, 2));
};


module.exports = { upVersion };
