test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect.hasAssertions();
  // BEGIN
  expect(target).toStrictEqual({ k: 'v', b: 'b', a: 'a' });
  expect(result).toStrictEqual({ k: 'v', b: 'b', a: 'a' });
  expect(target).toBe(result);
});

test('object assign changes target object', () => {
  const target = { };
  const source = { a: 1 };
  const result = Object.assign(target, source);
  expect(target).toBe(result);
});

test('object assign should merging objects', () => {
  const target = { a: 1 };
  const source1 = { b: 2 };
  const source2 = { c: 3 };
  const result = Object.assign(target, source1, source2);

  expect(result).toStrictEqual({ a: 1, b: 2, c: 3 });
});

test('object assign should merging objects with the same properties', () => {
  const target = { a: 1 };
  const source1 = { a: 2 };
  const source2 = { a: 3 };
  const result = Object.assign(target, source1, source2);

  expect(result).toStrictEqual({ a: 3 });
});

test('object assign should copy symbols', () => {
  const target = { };
  const source = {
    [Symbol('x')]: 2,
  };
  const result = Object.assign(target, source);

  expect(result).toStrictEqual(source);
});

test('should not copy inherited props', () => {
  const target = { };
  const source = Object.create({ foo: 1 });
  const result = Object.assign(target, source);

  expect(result).toStrictEqual({ });
});

test('should not copy not enumerable props', () => {
  const target = { };
  const source = Object.create(
    {},
    {
      x: {
        value: 1,
      },
      y: {
        value: 100,
        enumerable: true,
      },
    },
  );
  const result = Object.assign(target, source);

  expect(result).toStrictEqual({ y: 100 });
});

test('should pass primitives, null and undefined', () => {
  const target = { };
  const source1 = '123';
  const source2 = null;
  const source3 = true;
  const source4 = undefined;
  const source5 = Symbol('foo');
  const result = Object.assign(target, source1, source2, source3, source4, source5);

  expect(result).toStrictEqual({ 0: '1', 1: '2', 2: '3' });
});

test('throws exception TypeError when tries to copy only readable', () => {
  const target = Object.defineProperty({}, 'foo', {
    value: 1,
    writable: false,
  });
  const source = { foo: 3 };
  const result = () => Object.assign(target, source);

  expect(result).toThrowError(TypeError);
});

test('copies getter result when copy object with getter', () => {
  const target = { };
  const source = {
    x: 2,
    get y() {
      return 100;
    },
  };
  const result = Object.assign(target, source);
  expect(result).toStrictEqual({ x: 2, y: 100 });
});
// END
