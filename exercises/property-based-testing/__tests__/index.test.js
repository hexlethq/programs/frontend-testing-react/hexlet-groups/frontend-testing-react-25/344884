const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('properties', () => {
  test('should be the same result with reversed array', () => {
    fc.assert(
      fc.property(
        fc.int16Array(),
        (array) => {
          const reversedArray = array.slice().reverse();
          expect(sort(array)).toStrictEqual(sort(reversedArray));
        },
      ),
    );
  });

  test('should sort from smallest to highest', () => {
    fc.assert(
      fc.property(
        fc.int16Array(),
        (array) => {
          expect(sort(array)).toBeSorted();
        },
      ),
    );
  });

  test('should sort === double sort', () => {
    fc.assert(
      fc.property(
        fc.int16Array(),
        (array) => {
          expect(sort(array)).toStrictEqual(sort(sort(array)));
        },
      ),
    );
  });
});
// END
